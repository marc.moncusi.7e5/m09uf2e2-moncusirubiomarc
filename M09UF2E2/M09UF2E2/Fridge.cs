﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace M09UF2E2
{
    class Fridge
    {
        private const int _beercapacity= 9;
        private int _beer = 0;
        private Mutex mut = new Mutex();
        public Fridge(int startingBeer)
        {
            _beer = startingBeer;
        }
        public void BeerDrinking(string human)
        {
            Random rand = new Random();
            int _thirst  = rand.Next(0,7);
            Console.WriteLine($"{human} wants {_thirst} beers");
            mut.WaitOne();
            Console.WriteLine($"{human} opens the fridge");
            if(_beer-_thirst < 0)
            {
                _thirst = _beer;
            }
            _beer = _beer - _thirst;
            Console.WriteLine($"{human} drinks {_thirst} beers");
            Console.WriteLine($"{_beer} beers left ");
            Console.WriteLine($"{human} closes the fridge");
            mut.ReleaseMutex();

        }
        public void FillFridge(string human)
        {
            Random rand = new Random();
            int _refill = rand.Next(0, 7);
            Console.WriteLine($"{human} wants to refill {_refill} beers");
            mut.WaitOne();
            Console.WriteLine($"{human} opens the fridge");
            if (_beer + _refill > _beercapacity)
            {
                _refill = _refill - (_beer + _refill - _beercapacity);
            }
            _beer = _beer + _refill;
            Console.WriteLine($"{human} refills {_refill} beers");
            Console.WriteLine($"{_beer} beers left ");
            Console.WriteLine($"{human} closes the fridge");
            mut.ReleaseMutex();
        }
    }
}

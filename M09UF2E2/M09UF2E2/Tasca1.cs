﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace M09UF2E2
{
    class Tasca1
    {
        private string[] strings = {
            "En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo que vivía un hidalgo de los de lanza en astillero, adarga antigua, rocín flaco y galgo corredor.",
            "Pues sepa Vuestra Merced, ante todas cosas, que a mí llaman Lázaro de Tormes, hijo de Tomé González y de Antona Pérez, naturales de Tejares, aldea de Salamanca.",
            "DON JUAN, con antifaz, sentado a una mesa escribiendo; BUTTARELLI Y CIUTTI, a un lado esperando. Al levantarse el telón, se ven pasar por la puerta del fondo Máscaras, Estudiantes y Pueblo con hachones, músicas, etc."
        };
        private Mutex mut = new Mutex();
        private const int _numThreads = 3;


        public void StringReading(int thread, string line)
        {
            Console.WriteLine($"Thread {thread} reading: ");
            string[] subs = line.Split(' ');
            foreach (var sub in subs)
            {
                Console.Write($"{sub} ");
                Thread.Sleep(500);
            }
            Console.WriteLine();
        }
        public void ThreadCreation()
        {
            for (int i = 0; i < _numThreads; i++)
            {
                Thread t = new Thread(
                    new ParameterizedThreadStart(ThreadAction)
                );
                t.Start(i);
            }
        }
        public void ThreadAction(object data)
        {
            int thread = Convert.ToInt32(data);
            mut.WaitOne();
            StringReading(thread, strings[thread]);
            mut.ReleaseMutex();
        }
    }
}

﻿using System;
using System.Threading;

namespace M09UF2E2
{
    class Program
    {
        string[] teachers = { "Carles", "Eduard", "Xavi", "Estibaliz" };
        const int numInteractions = 3;
        int startingBeer = 6;
        static void Main(string[] args)
        {
            // Tasca 1
            //Tasca1 t1 = new Tasca1();
            //t1.ThreadCreation();

            // Tasca 2
            Program pr = new Program();
            pr.Tasca2();


        }
        public void Tasca2()
        {
            Fridge fr = new Fridge(startingBeer);

            for (int i = 0; i < teachers.Length; i++)
            {
                Thread newThread = new Thread(RandomAction);
                newThread.Name = String.Format($"{teachers[i]}");
                newThread.Start(fr);
            }
        }
        public void RandomAction(Object obj)
        {
            Fridge fr = (Fridge)obj;
            Random rand = new Random();
            for (int i = 0; i < numInteractions; i++)
            {
                
                switch (rand.Next(0, 2))
                {
                    case 0:
                        fr.BeerDrinking(Thread.CurrentThread.Name);
                        break;
                    case 1:
                        fr.FillFridge(Thread.CurrentThread.Name);
                        break;

                }
            }
        }
    }
}
